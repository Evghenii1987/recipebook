<#import "parts/common.ftl" as c>
<#import "parts/create-recipe.ftl" as cr>

<@c.page>


<div class="main_container2">
    <form   action="create-recipe"  object="recipe" method="post" enctype="multipart/form-data">
        <div><h2>title</h2><input type="text" field="title" id="title"
                                               placeholder="Enter here your title" required></div>
        <div><h2>description</h2><textarea rows="20" cols="150" name="comment"
                                                         field="description"></textarea></div>
        <div>
            <input type="file" name="file">
        </div>
        <div class="button">
            <button type="submit" formmethod="post" class="btn">Complete</button>
        </div>
    </form>
</div>
<#--    <@c.stily/>-->
</@c.page>