<#macro login path >

    <form action="${path}" method="post" novalidate>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label"><h2>Email</h2></label>
            <div class="col-sm-10">
                <input type="email" name="email" class="form-control"
                       placeholder="Enter here your email" required/>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label"><h2>Password</h2></label>
            <div class="col-sm-10">
                <input type="password" name="password" class="form-control"
                       placeholder="Enter here your password"/>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <a href="signup">Add new user</a>
                <button type="submit" class="btn btn-primary">Sign in</button>
            </div>
        </div>

    </form>
</#macro>

<#macro loguot exit>
    <form action="${exit}" method="post">
        <button class="btn btn-primary" type="submit">Sing Out</button>
    </form>
</#macro>