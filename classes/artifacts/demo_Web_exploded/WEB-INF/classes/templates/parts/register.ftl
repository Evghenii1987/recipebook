<#macro register regi>
    <form   action="${regi}" method="post">
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label"><h2>Email</h2></label>
            <div class="col-sm-10">
                <input type="email" name="email" class="form-control"
                       placeholder="Enter here your email" required/>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label"><h2>Password</h2></label>
            <div class="col-sm-10">
                <input type="password" name="password" class="form-control"
                       placeholder="Enter here your password" required/>
            </div>
        </div
        ><div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label"><h2>Name</h2></label>
            <div class="col-sm-10">
                <input type="text" name="name" class="form-control"
                       placeholder="Enter here your name" required/>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label"><h2>Surname</h2></label>
            <div class="col-sm-10">
                <input type="text" name="surname" class="form-control"
                       placeholder="Enter here your surname" required/>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label"><h2>Number</h2></label>
            <div class="col-sm-10">
                <input type="text" name="number" class="form-control"
                       placeholder="Enter here your number"/>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Sign in</button>
            </div>
        </div>

<#--    </form>-->
<#--        <div><lable><h2>Email</h2><input type="email" name="email" placeholder="Enter here your email" required></lable></div>-->
<#--        <div><label><h2>Password</h2><input type="password" name="password"-->
<#--                                            placeholder="Enter here your password" required></label></div>-->
<#--        <div><label><h2>Name</h2><input type="text" name="name" placeholder="Enter here your name" required></label></div>-->
<#--        <div><label><h2>Surname</h2><input type="text" name="surname" placeholder="Enter here your surname" required></label></div>-->
<#--        <div><label><h2>Number</h2><input type="text" name="telephoneNumber"-->
<#--                                          placeholder="Enter here your telephone number" required></label></div>-->

<#--        <div class="button">-->
<#--            <button type="submit" formmethod="post" class="btn">Register</button>-->
<#--        </div>-->
<#--    </form>-->
</#macro>