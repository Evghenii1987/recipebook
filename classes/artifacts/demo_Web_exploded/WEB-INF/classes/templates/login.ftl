<#import "parts/common.ftl" as c>
<#import "parts/login.ftl" as l>

<@c.page>

<div>
    <h1>Hello, User!</h1>
    <h2>Please LogIn!</h2>
</div>
<div>
<@l.login "login" />
</div>
</@c.page>
