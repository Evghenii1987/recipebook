<#import "parts/common.ftl" as c>


<@c.page>
    List of Users
    <table>
        <thead>
        <tr>
            <th>Name</th>
            <th>Role</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <#list users as user>
            <tr>
                <td>${user.name}</td>
                <td><#list user.roles as role>${role}<#sep>, </#list ></td>
                <td><a href="/user/${user.id}">Edit</a> </td>
            </tr>
        </#list>
        </tbody>
    </table>
     <span><a href="/welcome">Welcome Page!</a></span>
</@c.page>