<#import "parts/common.ftl" as c>
<#import "parts/recipe.ftl" as r>


<@c.page>
    <@r.recipe/>

    <h1>Welcome</h1>
    <div class="form-row">
        <div class="form-group col-md-6">
            <form method="get" action="recipe" class="form-inline">
                <input type="text" name="filter" class="form-control" value="${filter!""}" placeholder="Найти">
                <button type="submit" class="btn btn-primary ml-2">Найти</button>
            </form>
        </div>
    </div>
    <div>
        <span><a href="/user">User List</a></span>
    </div>
    <div>
        <form method="post" enctype="multipart/form-data">
            <input type="text" name="text" placeholder="Введите сообщение"/>

            <button type="submit">Добавить</button>
        </form>
    </div>

<#--    <div class="card-columns">-->
<#--        <#list recipes as mesagge>-->
<#--            <div class="card my-3">-->
<#--                <#if mesagge.fileName??>-->
<#--                    <img src="/img/${mesagge.fileName}" class="card-img-top">-->
<#--                </#if>-->
<#--                <div class="m-2">-->
<#--                <i>${mesagge.title}</i>-->
<#--                <span>${mesagge.description}</span>-->
<#--                </div>-->
<#--                <div class="card-footer text-muted">-->
<#--                <strong>${mesagge.creator}</strong>-->
<#--                </div>-->
<#--            </div>-->
<#--        <#else >-->
<#--            No Recipe-->
<#--        </#list>-->
<#--    </div>-->
</@c.page>