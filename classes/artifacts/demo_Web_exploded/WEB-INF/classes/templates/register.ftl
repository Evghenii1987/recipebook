<#import "parts/common.ftl" as c>
<#import "parts/register.ftl" as r>

<@c.page>
<div class="mb-2">
    Add NEW USER!
</div>
    ${message?ifExists}
<div >
    ${error?ifExists}
    <@r.register "register" />
</div>
<#--    <@c.stily/>-->
</@c.page>