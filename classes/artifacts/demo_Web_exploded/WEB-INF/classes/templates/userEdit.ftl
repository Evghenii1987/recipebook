<#import "parts/common.ftl" as c>


<@c.page>
List Editer
    <form action="/user" method="post">
        <input type="text" name="name" value="${user.name}">
        <#list roles as role>
            <div>
                <label><input type="checkbox" name="${role}" ${user.roles?seq_contains(role)?string("checked", "")}>${role}</label>
            </div>
        </#list>
        <input type="hidden" value="${user.id}" name="userId">
        <button type="submit">Save</button>
    </form>

</@c.page>