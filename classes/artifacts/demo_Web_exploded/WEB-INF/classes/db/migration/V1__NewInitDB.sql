create sequence hibernate_sequence start 1 increment 1;
create table t_recipe
(
    id          int8 not null,
    description varchar(2048)not null,
    file_name   varchar(255),
    title       varchar(255)not null,
    user_id     int8,
    primary key (id)
);
create table t_user
(
    user_id          int8 not null,
    email            varchar(255) not null,
    name             varchar(255)not null,
    password         varchar(255)not null,
    surname          varchar(255),
    telephone_number varchar(255),
    primary key (user_id)
);
create table user_role
(
    user_id int8 not null,
    roles   varchar(255)
);
alter table t_recipe
    add constraint recipe_fk foreign key (user_id) references t_user;
alter table user_role
    add constraint user_role_fk foreign key (user_id) references t_user;
--
-- insert into T_User (user_id, email, password, name, surname, telephone_number)
-- values (3, 'admin@mail.ru', 'admin', 'Admin', 'A', '123321');
--
-- insert into user_role(user_id, roles)
-- values (3, 'USER'),
--        (3, 'ADMIN');