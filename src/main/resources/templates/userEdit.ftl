<#import "parts/common.ftl" as c>


<@c.page>
    <h2> List Editer</h2>
    <form action="/user" method="post">
        <table class="table">
            <thead class="thead- dark">
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Role</th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td scope="row"><input type="text" name="name" value="${user.username}"></td>
                <#list roles as role>

                    <td scope="row">
                        <div>
                            <label><input type="checkbox"
                                          name="${role}" ${user.roles?seq_contains(role)?string("checked", "")}>${role}
                            </label>
                        </div>
                    </td>
                </#list>
                <td scope="row"><input type="hidden" value="${user.id}" name="userId"></td>
                <td>
                    <button type="submit">Save</button>
                </td>
            </tr>
            </tbody>
        </table>
    </form>

</@c.page>