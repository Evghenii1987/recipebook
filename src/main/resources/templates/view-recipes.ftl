<#import "parts/common.ftl" as c>
<#import "parts/recipe.ftl" as r>
<@c.page>
    <@r.recipe/>
    <div class="card-columns">
        <#list recipe as mesage>
            <div class="card my-3">
                <#if mesage.fileName??>
                    <img src="/uploads/${mesage.fileName}" class="card-img-top"/>
                </#if>
                <div class="m-2">
                    <i>${mesage.title}</i>
                </div>
                <div class="dropdown">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        Description
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <strong class="dropdown-item-text">${mesage.description}</strong>
                    </div>
                </div>
                <div class="card-footer text-muted">
                    <strong>${mesage.creator.username}</strong>
                </div>
            </div>
        <#else >
            No Recipe
        </#list>
    </div>
</@c.page>