<#import "parts/common.ftl" as c>
<#import "parts/create-recipe.ftl" as cr>

<@c.page>
    <div class="position-absolute w-90 p-3 mx-0  bg-secondary">

        <div class="main_container2 ">
            <form action="create-recipe" method="post" enctype="multipart/form-data">
                <div><h2>Title</h2><input type="text" name="title"
                                          placeholder="Enter here your title" required></div>
                <div><h2>Description</h2><textarea rows="15" cols="130" name="description"
                                                   placeholder="Enter here your descriprion" required></textarea></div>
                <div>
                    <input type="file" name="file">
                </div>
                <div class="col-sm-10 py-3 px-0 ">
                    <button type="submit" class="btn btn-primary">Complete</button>
                </div>

            </form>
        </div>
    </div>
<#--    <@c.stily/>-->
</@c.page>