<#macro login path >
<div class="position-absolute w-50 p-3 mx-2  bg-secondary">
    <div>
        <h1>Hello, dear friend, Please Sign In!</h1>
    </div>
    <form action="${path}" method="post" class="border border-success" novalidate>
        <div class="form-group row m-2 p-auto">
            <label for="inputEmail3" class="col-sm-2 col-form-label"><h4>Email</h4></label>
            <div class="col-sm-10 " >
                <input type="email" name="email" class="form-control"
                       placeholder="Enter here your email" required/>
            </div>
        </div>
        <div class="form-group row m-2 p-auto " >
            <label for="inputEmail3" class="col-sm-2 col-form-label"><h4>Password</h4></label>
            <div class="col-sm-10">
                <input type="password" name="password" class="form-control"
                       placeholder="Enter here your password"/>
            </div>
        </div>
        <div class="form-group row m-2 p-auto">
            <div class="col-sm-10 ">
                <a href="signup" class="text-success">Add new user</a>
                <button type="submit" class="btn btn-primary">Sign In</button>
            </div>
        </div>

    </form>
</div>
</#macro>

<#macro loguot exit>
    <form action="${exit}" method="post">
        <button class="btn btn-primary" type="submit">Sing Out</button>
    </form>
</#macro>