<#macro register regi>
    <div class="position-absolute w-50 p-3 mx-5  bg-secondary">
    <div>
        <h1>Please Register!</h1>
    </div>
    <form   action="${regi}" method="post" class="border border-success" >
        <div class="form-group row m-2 p-auto">
            <label for="inputEmail3" class="col-sm-2 col-form-label"><h4>Email</h4></label>
            <div class="col-sm-10">
                <input type="email" name="email" class="form-control"
                       placeholder="Enter here your email" required/>
            </div>
        </div>
        <div class="form-group row m-2 p-auto">
            <label for="inputEmail3" class="col-sm-2 col-form-label"><h4>Password</h4></label>
            <div class="col-sm-10">
                <input type="password" name="password" class="form-control"
                       placeholder="Enter here your password" required/>
            </div>
        </div
        ><div class="form-group row m-2 p-auto">
            <label for="inputEmail3" class="col-sm-2 col-form-label"><h4>Name</h4></label>
            <div class="col-sm-10">
                <input type="text" name="name" class="form-control"
                       placeholder="Enter here your name" required/>
            </div>
        </div>
        <div class="form-group row m-2 p-auto">
            <label for="inputEmail3" class="col-sm-2 col-form-label"><h4>Surname</h4></label>
            <div class="col-sm-10">
                <input type="text" name="surname" class="form-control"
                       placeholder="Enter here your surname" required/>
            </div>
        </div>
        <div class="form-group row m-2 p-auto">
            <label for="inputEmail3" class="col-sm-2 col-form-label"><h4>Number</h4></label>
            <div class="col-sm-10">
                <input type="text" name="number" class="form-control"
                       placeholder="Enter here your number"/>
            </div>
        </div>
        <div class="form-group row m-2 p-auto">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Sign in</button>
            </div>
        </div>

<#--    </form>-->
<#--        <div><lable><h2>Email</h2><input type="email" name="email" placeholder="Enter here your email" required></lable></div>-->
<#--        <div><label><h2>Password</h2><input type="password" name="password"-->
<#--                                            placeholder="Enter here your password" required></label></div>-->
<#--        <div><label><h2>Name</h2><input type="text" name="name" placeholder="Enter here your name" required></label></div>-->
<#--        <div><label><h2>Surname</h2><input type="text" name="surname" placeholder="Enter here your surname" required></label></div>-->
<#--        <div><label><h2>Number</h2><input type="text" name="telephoneNumber"-->
<#--                                          placeholder="Enter here your telephone number" required></label></div>-->

<#--        <div class="button">-->
<#--            <button type="submit" formmethod="post" class="btn">Register</button>-->
<#--        </div>-->
<#--    </form>-->
</#macro>