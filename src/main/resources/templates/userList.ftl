<#import "parts/common.ftl" as c>


<@c.page>
  <h2>  List of Users</h2>
    <table class="table">
        <thead class="thead- dark">
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Role</th>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <#list users as user>
            <tr>
                <td scope="row">${user.username}</td>
                <td scope="row"><#list user.roles as role>${role}<#sep>, </#list ></td>
                <td scope="row"><a href="/user/${user.id}">Edit</a> </td>
                <td scope="row"><a href="/user/${user.id}">Delete</a> </td>
            </tr>
        </#list>
        </tbody>
    </table>
     <span><a href="/welcome">Welcome Page!</a></span>
</@c.page>