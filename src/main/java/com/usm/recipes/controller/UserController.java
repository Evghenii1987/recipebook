package com.usm.recipes.controller;

import com.usm.recipes.Service.UserService;
import com.usm.recipes.dao.UserDao;
import com.usm.recipes.model.User;
import com.usm.recipes.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;
import java.util.Map;


@Controller
public class UserController {
    @Autowired
    private UserService userService;


    @GetMapping({"/", "/login"})
    public String login() {
        return "login";
    }


    @GetMapping("/view-recipes")
    public String welcome(Principal principal, Model model) {
        User user = userService.getUsersByEmail(principal.getName());
        model.addAttribute("user", user);
        return "recipe";
    }

    @GetMapping("/signup")
    public String signup(Model model) {
        model.addAttribute("user", new User());
        return "register";
    }

    @PostMapping("/register")
    public String register(User user, Map<String, Object> map) {

        if (!userService.addUser(user)) {
            map.put("error", "User with so e-mail exist!");
            return "register";
        }

        return "redirect:/login";
    }

    @GetMapping("/activate/{code}")
    public String activate(Model model, @PathVariable String code) {
        boolean isActivate = userService.activateUser(code);

        if (isActivate) {
            model.addAttribute("message", "User successfully activate!");
        } else {
            model.addAttribute("message", "Activation code is not found.");
        }

        return "login";
    }



}

