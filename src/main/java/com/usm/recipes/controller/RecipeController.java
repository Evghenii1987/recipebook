package com.usm.recipes.controller;

import com.usm.recipes.Service.UserService;
import com.usm.recipes.dao.RecipeDao;
import com.usm.recipes.model.Recipe;
import com.usm.recipes.model.User;
import com.usm.recipes.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.util.Map;
import java.util.UUID;

@Controller
public class RecipeController {
    @Autowired
    private RecipeService recipeService;
    @Autowired
    private UserService userService;
    @Autowired
    private RecipeDao recipeDao;
    @Value("${upload.path}")
    private String uploadPath;

    @GetMapping("/create-recipe")
    public String enterRecipe(Model model) {
        model.addAttribute("recipe", new Recipe());
        return "create-recipe";
    }

    @GetMapping("/welcome")
    public String recipe(@RequestParam(required = false, defaultValue = "") String filter, Model model){
       Iterable<Recipe> recipe= recipeDao.findAll();

        if ( filter !=null && !filter.isEmpty()){
            recipe=recipeDao.getRecipesByTitle(filter);
        }else{
            recipe=recipeDao.findAll();
        }
        model.addAttribute("recipe" ,recipe);
        model.addAttribute("filter",filter);
        return "recipe";
    }

    @PostMapping("/create-recipe")
    public String registerRecipe(@ModelAttribute( "recipe") Recipe recipe, Principal principal,Model models,
                                 @AuthenticationPrincipal User creator,
                                 @RequestParam String  description,
                                 @RequestParam String title, Map<String, Object> model,
                                 @RequestParam("file") MultipartFile file) throws IOException {

        User user = userService.getUsersByEmail(principal.getName());
        recipe.setCreator(user);
        Recipe recipes = new Recipe( title,  description, creator);
        if (file != null) {
            File upload = new File(uploadPath);
            if (!upload.exists()) {
                upload.mkdir();
            }

            String uuidFile = UUID.randomUUID().toString();
            String resultFileNam = uuidFile + "." + file.getOriginalFilename();

            file.transferTo(new File(uploadPath+"/"+resultFileNam));

            recipe.setFileName(resultFileNam);
        }
        recipeService.saveOrUpdate(recipe);
        models.addAttribute("user", user);
        model.put("recipes", recipeService.getAllRecipesByUser(user));
        return "redirect:welcome";
    }


    @GetMapping("/myRecipes")
    public String myRecipes(@ModelAttribute("recipe") Recipe recipe,Principal principal, Model model){
        User user= userService.getUsersByEmail(principal.getName());
        model.addAttribute("user", user);
        model.addAttribute("recipe",recipeService.getAllRecipesByUser(user));
        return "view-recipes";
    }


}
