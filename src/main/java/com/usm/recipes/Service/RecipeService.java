package com.usm.recipes.service;

import com.usm.recipes.model.Recipe;
import com.usm.recipes.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface RecipeService {
    List<Recipe> getAllRecipes();

    List<Recipe> getAllRecipesByUser(User creator);
   Recipe getRecipesById(Long id);
    void saveOrUpdate(Recipe recipe);
    void deleteRecipe(Long id);

}
