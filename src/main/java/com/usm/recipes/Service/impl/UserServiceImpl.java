package com.usm.recipes.service.impl;

import com.usm.recipes.Service.UserService;
import com.usm.recipes.dao.UserDao;
import com.usm.recipes.model.Role;
import com.usm.recipes.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private UserService userService;

    @Autowired
    private com.usm.recipes.Service.impl.MailSender mailSender;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void registerUser(String email, String password, String name, String surname, String telephoneNumber) {
        User user = new User(email, password, name, surname, telephoneNumber);
        userDao.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        return (List<User>) userDao.findAll();
    }

    @Override
    public User getUserById(Long id) {
        return userDao.findById(id).get();
    }

    @Override
    public boolean addUser(User user) {
        User userDB= userService.getUsersByEmail(user.getEmail());

        if(userDB!=null){
            return false;
        }

        user.setRoles(Collections.singleton(Role.USER));
        user.setActivationCode(UUID.randomUUID().toString());

        userService.saveOrUpdateUser(user);

        if (!StringUtils.isEmpty(user.getEmail())){
            String message= String.format(
                    "Hello %S!\n"+
                            "Welcome to RecipeBook! Please follow this link: http://localhost:8180/activate/%s",
                    user.getUsername(),
                    user.getActivationCode()

            );
            mailSender.sendMail(user.getEmail(),"Active code", message);
        }
        return true;
    }

    @Override
    public void save(User user) {
        userDao.save(user);
    }

    @Override
    public void saveOrUpdateUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

        userDao.save(user);
    }

    @Override
    public User getUsersByEmail(String email) {
        return userDao.findByEmail(email);
    }

    @Override
    public void deleteUser(Long id) {
        userDao.deleteById(id);
    }

    @Override
    public boolean isEmailUnique(String email) {
        if (getUsersByEmail(email) == null) {

            return true;
        } else {

            return false;
        }
    }

    @Override
    public boolean activateUser(String code) {
       User user=  userDao.findByActivationCode(code);

       if (user==null){
           return false;
       }

       user.setActivationCode(null);
       userDao.save(user);


        return true;
    }
}
