package com.usm.recipes.service.impl;

import com.usm.recipes.dao.RecipeDao;
import com.usm.recipes.model.Recipe;
import com.usm.recipes.model.User;
import com.usm.recipes.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RecipeServiceImpl implements RecipeService {

    @Autowired
    private RecipeDao recipeDao;
    @Override
    public List<Recipe> getAllRecipes() {
        return (List<Recipe>) recipeDao.findAll();
    }

    @Override
    public List<Recipe> getAllRecipesByUser(User creator) {
        return recipeDao.getAllRecipesByCreator(creator);
    }

    @Override
    public Recipe getRecipesById(Long id) {
        return recipeDao.findById(id).get();
    }

    @Override
    public void saveOrUpdate(Recipe recipe) {
        recipeDao.save(recipe);
    }

    @Override
    public void deleteRecipe(Long id) {

    }


}
