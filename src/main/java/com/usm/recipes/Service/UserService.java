package com.usm.recipes.Service;

import com.usm.recipes.model.User;

import java.util.List;

public interface UserService {

    void registerUser(String email, String password, String name, String surname, String telephoneNumber);

    List<User> getAllUsers();

    User getUserById(Long id);

   boolean addUser(User user);

    void save(User user);
    void saveOrUpdateUser(User user);

    User getUsersByEmail(String email);

    void deleteUser(Long id);

    public boolean isEmailUnique(String email);

    boolean activateUser(String code);
}
