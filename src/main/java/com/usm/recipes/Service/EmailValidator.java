package com.usm.recipes.Service;

public interface EmailValidator {
     boolean isValidEmail(String email);
}
