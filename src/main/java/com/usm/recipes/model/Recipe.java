package com.usm.recipes.model;

import javax.persistence.*;

@Entity
@Table(name = "T_Recipe")
public class Recipe {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;


    @Column(columnDefinition ="TEXT",name = "description")
    private String description;

    @Column(name = "title")
    private String title;

    private String fileName;


    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User creator;

    public Recipe(String title, String description,  User creator) {

        this.title=title;
        this.description = description;
        this.creator = creator;
    }
    public Recipe(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle(){return title;}

    public void setTitle(String title){this.title=title;}

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

}
